/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "oled.h"
#include "sht3x.h"
#include "i2c_hal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t  ShowMode = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void show_temp_humi(void)
{
	float fTemp = 0;
	float fHumi = 0;
	etError err = NO_ERROR;
	
	uint8_t strBuf[17] = {0};
	uint8_t show_str[17] = {0};
	int     iLen = 0;
	
	err = SHT3X_GetTempAndHumi(&fTemp, &fHumi, REPEATAB_HIGH, MODE_CLKSTRETCH, 50);
	printf("Temp = %.2f, Humi = %.2f, errcode = %02x\n", fTemp, fHumi, err);
	memset(strBuf, 0, sizeof(strBuf));
	memset(show_str, ' ', sizeof(show_str));
	sprintf((char *)strBuf, "T=%.0fC,H=%.0f%%", fTemp, fHumi);
	if(err == NO_ERROR){
		iLen = strlen((char *)strBuf);
		memcpy(show_str+(16-iLen)/2, strBuf, iLen);
		OLED_ShowString(0,2, show_str, 16);
		//OLED_ShowString(0,3, "1234567890123456", 16);
	}
}

void show_bat_volt(void)
{
	float bat_volt = 0;
	
	uint8_t strBuf[17] = {0};
	uint8_t show_str[17] = {0};
	int     iLen = 0;  

	bat_volt = adc_get_battery_voltage();

	printf("BAT volt: %.2fV\n", bat_volt);
	
	memset(strBuf, 0, sizeof(strBuf));
	memset(show_str, ' ', sizeof(show_str));
	sprintf((char *)strBuf, "BAT=%.2fV", bat_volt);
	iLen = strlen((char *)strBuf);
	memcpy(show_str+(16-iLen)/2, strBuf, iLen);
	OLED_ShowString(0,2, show_str, 16);
}

void show_light_intensity(void)
{
	float light = 0;
	
	uint8_t strBuf[17] = {0};
	uint8_t show_str[17] = {0};
	int     iLen = 0; 
	
	light = adc_get_light_intensity();
	
	printf("Light intensity value: %.0f lux\n", light);
	
	memset(strBuf, 0, sizeof(strBuf));
	memset(show_str, ' ', sizeof(show_str));
	sprintf((char *)strBuf, "Light=%.0f lux", light);
	iLen = strlen((char *)strBuf);
	memcpy(show_str+(16-iLen)/2, strBuf, iLen);
	OLED_ShowString(0,2, show_str, 16);

}

void show_sound_sensor(void)
{
	uint32_t sound = 0;
	
	uint8_t strBuf[17] = {0};
	uint8_t show_str[17] = {0};
	int     iLen = 0;
	
	sound = adc_get_sound_intensity();
	
	printf("sound sensor value: %d\n", sound);
	
	memset(strBuf, 0, sizeof(strBuf));
	memset(show_str, ' ', sizeof(show_str));
	sprintf((char *)strBuf, "Sound=%d", sound);
	iLen = strlen((char *)strBuf);
	memcpy(show_str+(16-iLen)/2, strBuf, iLen);
	OLED_ShowString(0,2, show_str, 16);
}

void show_all(void)
{
	float  bat_volt = 0;
	float  light = 0;
	uint32_t sound = 0;
	
	float fTemp = 0;
	float fHumi = 0;
	
	uint8_t strBuf[17] = {0};
	
	OLED_ShowCHinese(0,0,0);	//��
	OLED_ShowCHinese(18,0,1);	//��
	OLED_ShowCHinese(36,0,2);	//��

	SHT3X_GetTempAndHumi(&fTemp, &fHumi, REPEATAB_HIGH, MODE_CLKSTRETCH, 50);
	
	bat_volt = adc_get_battery_voltage();
	light    = adc_get_light_intensity();
	sound    = adc_get_sound_intensity();
	
	memset(strBuf, 0, sizeof(strBuf));
	sprintf((char *)strBuf, "V:%.1fV", bat_volt);
	OLED_ShowString(0,3, strBuf, 8);
	
	memset(strBuf, 0, sizeof(strBuf));
	sprintf((char *)strBuf, "T:%.0fC", fTemp);
	memset(strBuf+strlen((char *)strBuf), ' ', 8-strlen((char *)strBuf));
	OLED_ShowString(64,0, strBuf, 8);
	
	memset(strBuf, 0, sizeof(strBuf));
	sprintf((char *)strBuf, "H:%.0f%%",fHumi);
	memset(strBuf+strlen((char *)strBuf), ' ', 8-strlen((char *)strBuf));
	OLED_ShowString(64,1, strBuf, 8);
	
	
	memset(strBuf, 0, sizeof(strBuf));
	sprintf((char *)strBuf, "S:%d", sound);
	memset(strBuf+strlen((char *)strBuf), ' ', 8-strlen((char *)strBuf));
	OLED_ShowString(64,2, strBuf, 8);
	
	memset(strBuf, 0, sizeof(strBuf));
	sprintf((char *)strBuf, "L:%.0f", light);
	memset(strBuf+strlen((char *)strBuf), ' ', 8-strlen((char *)strBuf));
	OLED_ShowString(64,3, strBuf, 8);

	printf("---------------------\n");
	printf("Temp    : %.2f��C\n",  fTemp);
	printf("Humi    : %.2f%%\n",  fHumi);
	printf("Volt    : %.2fV\n", bat_volt);
	printf("Light   : %.0f lux\n", light);
	printf("sound   : %d\n", sound);
	printf("---------------------\n");
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint32_t cur_time = 0, last_time = 0;
	uint8_t show_flag = 0;
	
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_TIM2_Init();
  MX_ADC1_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  
  USART_EnableIT();
  printf("KE1 ADC Example ! \n");
	
  OLED_Init();
  OLED_Clear();

  SHT3X_SetI2cAdr(0x44);
	
  adc_start();
	
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		if (ShowMode == 1)
		{
				cur_time = HAL_GetTick();
				
				if(cur_time > last_time + 2000)
				{
					last_time = cur_time;
					OLED_ShowCHinese(36,0,0);	//��
					OLED_ShowCHinese(54,0,1);	//��
					OLED_ShowCHinese(72,0,2);	//��
					
					switch(show_flag)
					{
						case 0:
							LEDRGB_R_OFF;
							LEDRGB_G_ON;
							LEDRGB_B_OFF;
							
							RELAY_1_TOGGLE;
							show_temp_humi();
							show_flag = 1;
							break;
						
						case 1:
							LEDRGB_R_OFF;
							LEDRGB_G_OFF;
							LEDRGB_B_ON;
						
							RELAY_2_TOGGLE;
							show_bat_volt();
							show_flag = 2;
							break;
						
						case 2:
							LEDRGB_R_ON;
							LEDRGB_G_OFF;
							LEDRGB_B_OFF;
						
							show_light_intensity();
							show_flag = 3;
							break;
						
						case 3:
							LEDRGB_R_ON;
							LEDRGB_G_ON;
							LEDRGB_B_ON;
						
							show_sound_sensor();
							show_flag = 0;
							break;
						
						default:
							LEDRGB_R_OFF;
							LEDRGB_G_OFF;
							LEDRGB_B_OFF;
							break;
					}
				}
				
		}
		else
		{
			show_all();
		}

		LED_STATE_TOGGLE;
		
		HAL_Delay(500);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
