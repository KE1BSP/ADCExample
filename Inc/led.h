#ifndef __LED_H
#define __LED_H

#include "main.h"

#define LED_STATE_ON      HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET)
#define LED_STATE_OFF     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET)
#define LED_STATE_TOGGLE  HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_4)

#endif

